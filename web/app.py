from flask import Flask, render_template, abort, request
import os

app = Flask(__name__)

# Obtain the path to the templates directory
cwd = os.getcwd()
PATH = cwd + "/templates"

# Function to check if a file exists
def file_exists(name):
	# Loop through the files in ./templates
	for root, dirs, files in os.walk(PATH):
		# If the name of the file specified in the URL is in templates return true
		if name in files:
			return True
		else:
			return False

@app.route("/")
@app.route("/<path:path>")
def hello(path=None):
	raw_uri = request.__dict__['environ']['RAW_URI']
	# Handle no specified path
	if path == None:
		return "Hello World!"

	# Handle forbidden requests
	elif not (path.endswith(".html") or path.endswith(".css")):
		abort(403)
	elif ".." in raw_uri or "~" in raw_uri or "//" in raw_uri:
		abort(403)

	# Render desired file
	elif file_exists(path):
		return render_template(path), 200

	# Handle non existent file
	else:
		abort(404)


@app.errorhandler(404)
def error_404(error):
	return render_template('404.html'), 404


@app.errorhandler(403)
def error_403(error):
	return render_template('403.html'), 403

if __name__ == "__main__":
	app.run(debug=True, host='0.0.0.0')
